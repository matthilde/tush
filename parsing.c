/*
** Tiny UNIX Shell by matthilde
**
** parsing.c
**
** Will parse your epic gamer shell command
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tush.h"

// TODO: yet another skill issue (segfault) when parsing the damn shit

#define TUSH_HEAP_SIZE 128
#define TUSH_STRING_HEAP_SIZE 128

TushToken tush_heap[TUSH_HEAP_SIZE];
char *tush_string_heap[TUSH_STRING_HEAP_SIZE] = { NULL };

size_t tush_heap_ptr = 0, tush_string_heap_ptr = 0;

static inline int tush_isvar(char c) {
    return c == '_' || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || \
           (c >= '0' && c <= '9');
}

static inline int tush_isspace(char c) {
    return c == ' ' || c == '\n' || c == '\t';
}

//// HEAP MANAGEMENT
void tush_free_all() {
    for (size_t i = 0; i < tush_string_heap_ptr; ++i)
        free(tush_string_heap[i]);
    tush_string_heap_ptr = tush_heap_ptr = 0;
}

static TushToken* tush_alloc() {
    return &tush_heap[tush_heap_ptr++];
}

static char *tush_alloc_str(size_t length) {
    char *buf = (char*)malloc((length + 1) * sizeof(char));
    if (!buf) {
        perror("tush: malloc");
        exit(1);
    }
    tush_string_heap[tush_string_heap_ptr++] = buf;
    return buf;
}
static char *tush_realloc_str(char *buf, size_t length) {
    long idx = -1;
    for (long i = 0; i < (long)tush_string_heap_ptr; ++i) {
        if (buf == tush_string_heap[i]) { idx = i; break; }
    }
    if (idx < 0 ||
        !(buf = (char*)realloc(buf, (length+1) * sizeof(char)))) {
        perror("tush: realloc");
        exit(1);
    }

    tush_string_heap[idx] = buf;
    return buf;
}

//// STRING FORMATTING
// String formatting with env variables
#define APPENDSZ(n) ({ used += n; for (; sz < used; sz *= 2 ) m = 1; \
            if (m) { buf = tush_realloc_str(buf, sz); m = 0; } })

// Formats strings accordingly
char *tush_env_fmt(const char *s) {
    char varbuf[64]; int i, m = 0;
    size_t sz = 1, used = 0;

    // Once upon a time... There used to have a memory leak there...
    char *buf = tush_alloc_str(sz);
    size_t bufptr = 0;

    buf[0] = 0;

    for (; *s; ++s) {
        if (*s == '$') {
            ++s;
            // Get environment variable
            for (i = 0; tush_isvar(*s) && i < 63; i++, s++) varbuf[i] = *s;
            --s;
            if (i == 0) {
                APPENDSZ(1);
                buf[bufptr++] = '$';
            } else {
                varbuf[i] = 0;

                char *var = getenv(varbuf);
                if (var) {
                    APPENDSZ(strlen(var));
                    memcpy(&buf[bufptr], var, strlen(var));
                    bufptr += strlen(var);
                }
            }
        }
        else if (*s != '"') {
            APPENDSZ(1);
            buf[bufptr++] = *s;
        }
    }
    buf[bufptr] = 0;
    return buf;
}
#undef APPENDSZ

//// STRING SPLITTING
static char* tush_strtok_ptr = NULL;

// imagine strtok but better (it can do string and stuf)
static char* tush_strtok(char* ptr) {
    if (ptr) tush_strtok_ptr = ptr;
    ptr = tush_strtok_ptr;
    for (; tush_isspace(*ptr) && *ptr; ++ptr);
    if (!(*ptr)) return NULL;

    int is_str = 0;
    char* startptr = ptr;
    for (; (!tush_isspace(*ptr) || is_str) && *ptr; ++ptr) {
        switch (*ptr) {
        case '"': is_str = !is_str; break;
        }
    }
    *ptr = 0;
    tush_strtok_ptr = ptr+1;
    return startptr;
}

//// COMMAND PARSING
// Recursively parses the command line and returns a tree.
// char *s will be altered.
TushToken *tush_parse(char *s) {
    size_t i;
    TushToken* cmd = tush_alloc();

    // First pass: Pipes
    for (i = 0; s[i]; ++i) {
        if (s[i] == '|') {
            cmd->type = TUSH_TYPE_PIPE;
            s[i] = 0;
            cmd->c.pipe.left = tush_parse(s);
            if (!(cmd->c.pipe.left)) return NULL;
            cmd->c.pipe.right = tush_parse(s+i+1);
            if (!(cmd->c.pipe.right)) return NULL;

            return cmd;
        }
    }

    // Second pass: read/write redirection
    for (i = 0; s[i]; ++i) {
        switch (s[i]) {
        case '>':
            cmd->type = TUSH_TYPE_WRITE;
            s[i] = 0;
            cmd->c.redirect.cmd = tush_parse(s);
            if (!(cmd->c.redirect.cmd)) return NULL;
            cmd->c.redirect.filename = strtok(s+i+1, " \t\r\n");
            break;
        case '<':
            cmd->type = TUSH_TYPE_READ;
            s[i] = 0;
            cmd->c.redirect.cmd = tush_parse(s);
            if (!(cmd->c.redirect.cmd)) return NULL;
            cmd->c.redirect.filename = strtok(s+i+1, " \t\r\n");
            break;
        default: continue;
        }

        return cmd;
    }

    // Parse command instead.
    cmd->type = TUSH_TYPE_CMD;

    char *tok = tush_strtok(s);
    if (!tok) return NULL;
    for (i = 0; tok && i < MAX_ARG_SIZE - 1; ++i) {
        cmd->c.cmd.argv[i] = tush_env_fmt(tok);
        tok = tush_strtok(NULL);
    }
    cmd->c.cmd.argv[i] = NULL;

    return cmd;
}

TushToken *tush_parse_str(char *s) {
    tush_free_all();
    return tush_parse(s);
}

//// DEBUGGING FUNCTIONS
// Shows tree for debugging
void tush_print_tree(TushToken *tok, int indent) {
    for (int i = 0; i < indent; ++i) printf("| ");
    switch (tok->type) {
    case TUSH_TYPE_CMD:
        for (int j = 0; tok->c.cmd.argv[j]; ++j)
            printf("%s ", tok->c.cmd.argv[j]);
        putchar('\n');
        break;
    case TUSH_TYPE_PIPE:
        puts("PIPE");
        tush_print_tree(tok->c.pipe.left, indent+1);
        tush_print_tree(tok->c.pipe.right, indent+1);
        break;
    case TUSH_TYPE_WRITE:
        printf("WRITE(%s)\n", tok->c.redirect.filename);
        tush_print_tree(tok->c.redirect.cmd, indent+1);
        break;
    case TUSH_TYPE_READ:
        printf("READ(%s)\n", tok->c.redirect.filename);
        tush_print_tree(tok->c.redirect.cmd, indent+1);
        break;
    }
}
