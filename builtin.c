/*
** Tiny UNIX Shell by matthilde
**
** builtin.c
**
** Built-in commands
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "tush.h"

struct _builtin_t {
    const char* name;
    int (*fn)(int, char**);
};

static int tush_builtin_cd(int argc, char **argv) {
    const char *dir = argc == 1 ? getenv("HOME") : argv[1];
    if (!dir || chdir(dir) < 0) {
        perror("cd");
        return 1;
    }
    return 0;
}

static int tush_builtin_pwd(int argc, char **argv) {
    (void)argc; (void)argv;
    char buf[512];

    if (!getcwd(buf, 512)) {
        perror("pwd");
        return 1;
    }
    puts(buf);
    return 0;
}

static int tush_builtin_export(int argc, char **argv) {
    if (argc != 2) return 1;

    char *eq = strchr(argv[1], '=');
    if (!eq) return 1;
    *eq = 0;

    if (setenv(argv[1], eq + 1, 1) < 0) {
        perror("export");
        return 1;
    }
    return 0;
}

static int tush_builtin_exit(int argc, char **argv) {
    if (argc > 1) exit(atoi(argv[1]));
    exit(0);
}

#define BUILTIN_SIZE 4
static struct _builtin_t builtins[BUILTIN_SIZE] = {
{ "cd", tush_builtin_cd },
{ "pwd", tush_builtin_pwd },
{ "export", tush_builtin_export },
{ "exit", tush_builtin_exit },
};

// Returns -1 on fail
int tush_builtin_exec(char **argv) {
    int argc;
    for (argc = 0; argv[argc]; ++argc);

    for (int i = 0; i < BUILTIN_SIZE; ++i)
        if (!strcmp(argv[0], builtins[i].name))
            return builtins[i].fn(argc, argv);
    return -1;
}
