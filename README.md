# tush - Tiny UNIX Shell

tush is a very simple and small shell for UNIX-like systems. It contains all the
basic stuff such as redirection, pipes and a few builtins.

## Features

- pipes and redirections
- `cd`, `pwd`, `export` and `exit` builtins
- custom prompt support
- a lot of bugs probably

## custom prompt

The custom prompt can contain the following formatting thingies:

- `%D` current working directory
- `%d` cwd but with shortens the home directory with a tilde
- `%h` the computer's hostname
- `%u` username
- `%%` escape sequence for the % character
- `%$` (i think it doesnt work because i need to redo the parser), displays an $ or # depending whether you're root or not

## Lil demo

[![asciicast](https://asciinema.org/a/eFLOwJb685jjkPyTdmdOqCJVi.svg)](https://asciinema.org/a/eFLOwJb685jjkPyTdmdOqCJVi)

## Licensing

public domain i guess
