#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#include "tush.h"

#define BUFFER_SIZE 1024

int tush_lastexit = 0;
int hasint = 0;

void bypass_sigint(int a) {
    hasint = 1;
    (void)a;
    // signal(SIGINT, bypass_sigint);
}

static void check_status(int wstatus) {
    if (WIFEXITED(wstatus)) tush_lastexit = WEXITSTATUS(wstatus);
    if (WIFSIGNALED(wstatus)) {
        fprintf(stderr, "tush: process terminated (%d)\n", WTERMSIG(wstatus));
        tush_lastexit = 128 + WTERMSIG(wstatus);
    }
}

static void enable_ctrlc_handle() {
    struct sigaction act = { 0 };
    act.sa_handler = bypass_sigint;
    sigaction(SIGINT, &act, NULL);
}

static void disable_ctrlc_handle() {
    struct sigaction act = { 0 };
    act.sa_handler = SIG_DFL;
    sigaction(SIGINT, &act, NULL);
}

static void ignore_ctrlc_handle() {
    struct sigaction act = { 0 };
    act.sa_handler = SIG_IGN;
    sigaction(SIGINT, &act, NULL);
}

void PROMPT(int interactive, char *cmd, FILE *f) {
    TushToken *t;
    char buf[BUFFER_SIZE]; int wstatus;
    tush_set_default_prompt();

    do {
        if (!cmd) {
            enable_ctrlc_handle(); // Enable on prompt
            if (interactive) printf("%s", tush_get_prompt());
            if (!fgets(buf, BUFFER_SIZE, f)) {
                if (hasint && f == stdin) {
                    hasint = 0;
                    if (interactive) putchar('\n');
                    continue;
                } else break;
            }

            t = tush_parse_str(buf);
        } else
            t = tush_parse_str(cmd);

        if (!t) continue;

        // Is that... a builtin!?!?!?!?
        if (t->type == TUSH_TYPE_CMD) {
            if (tush_builtin_exec(t->c.cmd.argv) >= 0) continue;
        }
        // No??? That must be an ACTUAL command then!
        ignore_ctrlc_handle(); // Disable while running the thing
        if (!fork()) {
            disable_ctrlc_handle();
            tush_eval(t);
            exit(0);
        }
        wait(&wstatus);
        check_status(wstatus);
    } while (!cmd);
}

static void usage() {
    fputs("USAGE: tush [FILE]\n"
          "       tush (-h|-v)\n"
          "       tush [-c command]\n", stderr);
}

int main(int argc, char **argv) {
    int opt; int interactive = 1;
    char* cmd = NULL; FILE *f = stdin;

    while ((opt = getopt(argc, argv, "hvc:")) != -1)
        switch (opt) {
        case 'h': usage(); exit(0); break;
        case 'v': fputs("tush by matthilde", stderr); exit(0); break;
        case 'c': cmd = optarg; interactive = 0; break;
        default: usage(); exit(EXIT_FAILURE); break;
        }

    if (optind < argc) {
        f = fopen(argv[optind], "r");
        if (!f) {
            perror("tush: popen");
            exit(EXIT_FAILURE);
        }
        interactive = 0;
    }

    if (!isatty(STDIN_FILENO) && f == stdin) interactive = 0;

    PROMPT(interactive, cmd, f);
    return 0;
}
