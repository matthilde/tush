/*
** Tiny UNIX Shell by matthilde
**
** eval.c
**
** Evaluates a token tree.
*/
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#include "tush.h"

// Implementing non-standard strdup
static char* tush_strdup(const char* s) {
    size_t l = strlen(s);
    char* d = (char*)malloc((l+1) * sizeof(char));
    strcpy(d, s);
    return d;
}

static inline int tush_exefile_available(const char* path) {
    return access(path, R_OK | X_OK);
}

static void tush_exe_from_path(char* argv[]) {
    char pathbuf[256], *ptok;
    char* path, *ename = argv[0];

    // Then try PATH, and then no PATH
    sprintf(pathbuf, "%s", ename);
    path = getenv("PATH");
    if (!strchr(ename, '/') && path) {
        path = tush_strdup(path);
        ptok = strtok(path, ":");
        while (ptok) {
            sprintf(pathbuf, "%s/%s", ptok, ename);
            if (tush_exefile_available(pathbuf) == 0) break;
            ptok = strtok(NULL, ":");
        }
        free(path);
    }

    if (execv(pathbuf, argv) < 0) {
        perror("execv");
        exit(1);
    }
}

static inline void tush_pipe(TushToken* left, TushToken* right) {
    int p[2];

    if (pipe(p) < 0) {
        perror("pipe");
        exit(1);
    }

    if (!fork()) {
        close(1);
        dup(p[1]);
        close(p[1]);
        close(p[0]);
        tush_eval(left);
    }

    close(0);
    dup(p[0]);
    close(p[0]);
    close(p[1]);
    tush_eval(right);
}

static inline void tush_redir_write(TushToken* cmd, char *filename) {
    int f = open(filename, O_CREAT | O_WRONLY, 420);
    if (f < 0) {
        perror("open");
        exit(1);
    }

    close(1);
    dup(f);
    close(f);
    tush_eval(cmd);
}

static inline void tush_redir_read(TushToken* cmd, char *filename) {
    int f = open(filename, O_RDONLY);
    if (f < 0) {
        perror("open");
        exit(1);
    }

    close(0);
    dup(f);
    close(f);
    tush_eval(cmd);
}

// -1 if fail
void tush_eval(TushToken* t) {
    switch (t->type) {
        case TUSH_TYPE_CMD:
            tush_exe_from_path(t->c.cmd.argv);
            break;
        case TUSH_TYPE_PIPE:
            tush_pipe(t->c.pipe.left, t->c.pipe.right);
            break;
        case TUSH_TYPE_WRITE:
            tush_redir_write(t->c.redirect.cmd, t->c.redirect.filename);
            break;
        case TUSH_TYPE_READ:
            tush_redir_read(t->c.redirect.cmd, t->c.redirect.filename);
            break;
    }
}
