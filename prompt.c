/*
** Tiny UNIX Shell by matthilde
**
** prompt.c
**
** Handles prompt formatting. Set $PS1 to modify it.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pwd.h>

#include "tush.h"

#define MAX_PROMPT_SIZE 255
#define TEMPBUF_SIZE 64

static char promptbuf[MAX_PROMPT_SIZE + 1];
static char tempbuf[TEMPBUF_SIZE];
static const char* defaultprompt = "%u@%h:%d %P ";
// In case something goes wrong...
static const char* fallbackprompt = "$ ";

void tush_set_default_prompt() {
    setenv("PS1", defaultprompt, 1);
}

// Enables ~ if shorten is > 0
static int tush_get_cwd(int shorten) {
    const char *homedir;
    if (!getcwd(tempbuf, TEMPBUF_SIZE)) return -1;
    if (shorten && (homedir = getenv("HOME"))) {
        if (strstr(tempbuf, homedir) == tempbuf) {
            memmove(&tempbuf[1], &tempbuf[strlen(homedir)],
                    strlen(&tempbuf[strlen(homedir)]) + 1);
            tempbuf[0] = '~';
        }
    }
    return 0;
}

static const char* tush_get_username() {
    struct passwd *pw;
    if (!(pw = getpwuid(geteuid()))) return NULL;
    return pw->pw_name;
}

// Returns the fallback prompt on fail
const char *tush_parse_prompt(const char* s) {
    size_t pbi = 0, slen = strlen(s), tsz;
    const char *ts;

    for (size_t i = 0; i < slen && pbi < MAX_PROMPT_SIZE; ++i) {
        if (s[i] == '%') {
            ts = tempbuf;
            switch (s[++i]) {
            case 'D': case 'd': // get cwd
                if (tush_get_cwd(s[i] == 'd') < 0) return fallbackprompt;
                tsz = strlen(tempbuf);
                break;
            case 'h': // hostname
                if (gethostname(tempbuf, TEMPBUF_SIZE) < 0)
                    return fallbackprompt;
                tsz = strlen(tempbuf);
                break;
            case 'r': // last status code
                tsz = sprintf(tempbuf, "%d", tush_lastexit);
                break;
            case 'u': // username
                if (!(ts = tush_get_username()))
                    return fallbackprompt;
                tsz = strlen(ts);
                break;
            case '%': promptbuf[pbi++] = '%'; continue; break;
            case 'P': // get prompt symbol
                if (!(ts = tush_get_username()))
                    return fallbackprompt;
                promptbuf[pbi] = strcmp(ts, "root") == 0 ? '#' : '$';
                pbi++;
                continue;
                break;
            default:
                return fallbackprompt;
                break;
            }
            if (pbi + tsz > MAX_PROMPT_SIZE)
                return fallbackprompt;
            strcpy(&promptbuf[pbi], ts);
            pbi += tsz;
        } else promptbuf[pbi++] = s[i];
    }
    promptbuf[pbi] = 0;

    return promptbuf;
}

/*
** %r - last status code
** %d - working directory
** %D - full working directory
** %P - prompt symbol ($ for user, # for root)
** %h - hostname
** %u - username
** %% - %
*/
const char *tush_get_prompt() {
    const char* prompt = defaultprompt;
    if (getenv("PS1")) prompt = getenv("PS1");

    return tush_parse_prompt(prompt);
}
