#ifndef TUSH_H_
#define TUSH_H_

#define MAX_ARG_SIZE 24

// parsing token
typedef struct TushToken {
    enum {
        TUSH_TYPE_CMD,
        TUSH_TYPE_PIPE,
        TUSH_TYPE_WRITE,
        TUSH_TYPE_READ,
    } type;

    union {
        struct {
            char *argv[MAX_ARG_SIZE];
        } cmd;
        struct {
            struct TushToken *left;
            struct TushToken *right;
        } pipe;
        struct {
            struct TushToken *cmd;
            char *filename;
        } redirect;
    } c;
} TushToken;

// main.c
extern int tush_lastexit;
// parsing.c
TushToken *tush_parse_str(char *s);
void tush_print_tree(TushToken *tok, int indent);
// eval.c
void tush_eval(TushToken* t);
// builtin.c
int tush_builtin_exec(char**);
// prompt.c
void tush_set_default_prompt();
const char *tush_parse_prompt(const char *s);
const char *tush_get_prompt();

#endif // TUSH_H_
